from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import time
import numpy as np

def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict


dataset1 = unpickle("/home/kristo/PycharmProjects/tefas_ergasia/cifar-10-batches-py/data_batch_1")
dataset2 = unpickle("/home/kristo/PycharmProjects/tefas_ergasia/cifar-10-batches-py/data_batch_2")
dataset3 = unpickle("/home/kristo/PycharmProjects/tefas_ergasia/cifar-10-batches-py/data_batch_3")
dataset4 = unpickle("/home/kristo/PycharmProjects/tefas_ergasia/cifar-10-batches-py/data_batch_4")
dataset5 = unpickle("/home/kristo/PycharmProjects/tefas_ergasia/cifar-10-batches-py/data_batch_5")

concatenated_data = np.concatenate([dataset1[b'data'], dataset2[b'data'], dataset3[b'data'], dataset4[b'data'], dataset5[b'data']], axis=0)

# Assuming each dataset is a dictionary with the key 'labels' for the corresponding labels
# Concatenate the labels along the first axis
concatenated_labels = np.concatenate([dataset1[b'labels'], dataset2[b'labels'], dataset3[b'labels'], dataset4[b'labels'], dataset5[b'labels']], axis=0)

X = concatenated_data
y = concatenated_labels

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Standardize the features by removing the mean and scaling to unit variance
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)


# Create an SVM classifier
svm_classifier = SVC(kernel='rbf', C=3)

# Measure the time taken for training
start_time = time.time()
# Train the classifier on the training data
svm_classifier.fit(X_train, y_train)
training_time = time.time() - start_time
print(f"Training time: {training_time:.4f} seconds")

# Measure the time taken for making predictions
start_time = time.time()
# Make predictions on the testing data
y_pred = svm_classifier.predict(X_test)
prediction_time = time.time() - start_time

print(f"Prediction time: {prediction_time:.4f} seconds")

# Evaluate the accuracy of the model
accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy:.2f}")
