import numpy as np
import matplotlib.pyplot as plt


def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict


a = unpickle("/home/kristo/PycharmProjects/tefas_ergasia/cifar-10-batches-py/data_batch_1")

print(len(a[b"data"][0]))

image1 = a[b"data"][0]

image1_r = image1[:1024]
image1_g = image1[1024:2048]
image1_b = image1[2048:]

# Reshape the array into a 32x32 matrix
reshaped_matrix_r = np.reshape(image1_r, (32, 32))
# Reshape the array into a 32x32 matrix
reshaped_matrix_g = np.reshape(image1_g, (32, 32))
# Reshape the array into a 32x32 matrix
reshaped_matrix_b = np.reshape(image1_b, (32, 32))

# Combine the three channels into an RGB image
rgb_image = np.stack([reshaped_matrix_r, reshaped_matrix_g, reshaped_matrix_b], axis=-1)

# Display the image using matplotlib
plt.imshow(rgb_image)
plt.axis('off')  # Optional: Turn off axis labels
plt.show()

label_image1 = a[b"labels"][0]

print(label_image1)


def image_show(vector):
    vector_r = vector[:1024]
    vector_g = vector[1024:2048]
    vector_b = vector[2048:]

    # Reshape the array into a 32x32 matrix
    reshaped_vector_r = np.reshape(vector_r, (32, 32))
    # Reshape the array into a 32x32 matrix
    reshaped_vector_g = np.reshape(vector_g, (32, 32))
    # Reshape the array into a 32x32 matrix
    reshaped_vector_b = np.reshape(vector_b, (32, 32))

    # Combine the three channels into an RGB image
    rgb_image = np.stack([reshaped_vector_r, reshaped_vector_g, reshaped_vector_b], axis=-1)

    # Display the image using matplotlib
    plt.imshow(rgb_image)
    plt.axis('off')  # Optional: Turn off axis labels
    plt.show()


for i in range(10):
    image_show(a[b"data"][i])
